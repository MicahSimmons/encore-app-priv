#!/usr/bin/env ruby
#
# Copyright:: (c) 2011-2015, Enphase Energy. All rights reserved.
#
# Opens a log file at the provided path and name.
#
# Logging class

# Log file definitions

require 'time'

class GenericMonitorLog
  attr_reader :io
  attr_reader :stamp
  attr_writer :stamp

  def initialize(log, stamp = true)
   @stamp = stamp
   @line == true
   open(log)
  end

  def open(log)
    @io = if log.is_a? IO
      log
    elsif log.is_a? GenericMonitorLog
      @stamp = log.stamp
      log.io
    elsif log.is_a? File
      File.new( log.path, "a" )
    else
      File.new( log, "a" )
    end
    self
  end

  def close
    entry("Log File Closed")
    @io.close
  end

  def entry(le, caller_depth = 0)
    pre = if stamp
      at = caller_locations()[caller_depth]
      "#{stampIt} :PRT: [#{File.basename(at.absolute_path)}:#{at.lineno} #{at.label}], "
    else
      ""
    end

    if le.is_a?(Exception)
      le = "#{le}:\n#{le.backtrace.join("\n")}"
    end

    @io.puts("#{pre}#{le}\n")
    @io.flush()
  end

  def sh(cmd, prefix = "", caller_depth = 3)
    IO.popen(cmd + " 2>&1") do |io|
      while line = io.gets
        entry("#{prefix}#{line.chomp}", caller_depth)
      end
    end
    return ($? == 0)
  end

  private

  def stampIt()
    return Time.now.strftime("[%Y/%m/%d %H:%M:%S.%6N]")
  end
end
