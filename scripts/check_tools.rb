#!/usr/bin/env ruby

# check_tools.rb
# Verify tool prerequisites are satisfied prior to starting a
# build operation.

require 'optparse'
require 'scanf'

g_build_platforms=[]
g_all_platforms=['host', 'envoy']


REQD_LEVEL_OPTIONAL = 0
REQD_LEVEL_HOST = 1
REQD_LEVEL_ENVOY = 2
REQD_LEVEL_ALL = 3

REQD_CHECK_SKIP = 0
REQD_CHECK_OK = 1
REQD_CHECK_FAIL_NOT_FOUND = -1
REQD_CHECK_FAIL_NO_VERSION = -2
REQD_CHECK_FAIL_OLD_VERSION = -3

class BuildTool
  def do_check(lvl)
    # Do we care?
    if @required_lvl != REQD_LEVEL_ALL
      return REQD_CHECK_SKIP if lvl != @required_lvl
    end

    print "Checking #{@exe_name} version #{@version_str}..."

    # Does the tool exist?
    path_to_tool = `which #{@exe_name}`
    return REQD_CHECK_FAIL_NOT_FOUND if path_to_tool.empty?

    # exe_name --version
    version_raw = `#{@exe_name} --version`
    version_string = version_raw.scan(/[0-9]+\.[0-9]+\.[0-9]+/).last
    return REQD_CHECK_FAIL_NO_VERSION if version_string.nil?
    print "got #{version_string}..."

    # check for matching version string
    major, minor, patch = version_string.scanf("%d.%d.%d")
    expected_major, expected_minor, expected_patch = @version_str.scanf("%d.%d.%d")

    return REQD_CHECK_FAIL_OLD_VERSION if (major < expected_major)
    return REQD_CHECK_FAIL_OLD_VERSION if (major == expected_major) && (minor < expected_minor)
    return REQD_CHECK_FAIL_OLD_VERSION if (major == expected_major) && (minor == expected_minor) && (patch < expected_patch)

    return REQD_CHECK_OK
  end

  def initialize (exe_name, version, required_level)
    @exe_name = exe_name
    @version_str = version
    @required_lvl = required_level
  end
end



####################################
# Main processing, option parsing
####################################
if __FILE__ == $PROGRAM_NAME
  options = {}
  OptionParser.new do |opts|
    opts.banner = "usage:  check_tools.rb --platform <build_platform>\n"
    opts.on('-p', '--platform [target]', "Platform to check. #{g_all_platforms} or \"all\"") do |arg|
      if g_all_platforms.include?(arg)
        g_build_platforms.push(arg)
      end
    end
  end.parse!

  if g_build_platforms.empty?
    puts "Required parameter missing \"--platform [host|envoy]\"\n"
    exit -1
  end

  case g_build_platforms.last
  when "host"
    needed_lvl = REQD_LEVEL_HOST
  when "envoy"
    needed_lvl = REQD_LEVEL_ENVOY
  else
    puts "Not a recognized platform type."
    exit -1
  end

  all_tools = []
  all_tools.push(BuildTool.new("gcc", "5.4.0", REQD_LEVEL_HOST))
  all_tools.push(BuildTool.new("g++", "5.4.0", REQD_LEVEL_HOST))
  all_tools.push(BuildTool.new("ruby", "2.1.5", REQD_LEVEL_ALL))
  all_tools.push(BuildTool.new("cmake", "3.8.1", REQD_LEVEL_ALL))
  all_tools.push(BuildTool.new("ccache", "3.2.4", REQD_LEVEL_ALL))
  all_tools.push(BuildTool.new("arm-linux-gnueabihf-gcc", "5.4.0", REQD_LEVEL_ENVOY))
  all_tools.push(BuildTool.new("arm-linux-gnueabihf-g++", "5.4.0", REQD_LEVEL_ENVOY))



  super_result = true
  all_tools.each do |tool|
    result = tool.do_check(needed_lvl)
    case result
    when REQD_CHECK_SKIP
      # nop
    when REQD_CHECK_OK
      puts "OK"
    else
      puts "FAIL"
      super_result = false
    end
  end

  if super_result == false
    puts "\n"
    puts "***************************************************\n"
    puts "ERROR: One or more tools have failed version check!\n"
    puts "Please verify toolchain installation for this\n"
    puts "build system.  YMMV.\n"
    puts "***************************************************\n"
    puts "\n"
    exit -1
  end
end

puts "All build tools verified, OK!"
exit 0
