#!/usr/bin/env ruby

require 'optparse'
require 'pathname'
require './scripts/gml'

####################################
# Global constants
####################################
BM_SUCCESS = true
BM_FAILURE = false


####################################
# TOOLS
####################################
CMAKE = "cmake"
MAKE = "make"


####################################
# Global variables
####################################
g_build_platforms=[]
g_all_platforms=['host', 'envoy']

g_build_stages=[]
g_all_stages=['clean', 'setup', 'make', 'pkg']

g_build_targets=[]
g_all_targets=['ut', 'pkg']

$log = GenericMonitorLog.new($stdout, true)
$top = File.dirname(Pathname.new(__FILE__).realpath)

####################################
# Build Manager class
#  This class implements the build operations for a unique
#  platform/target pair.
#
# Member functions:
#   clean() - destroy a prior build tree
#   setup() - Create the build tree, generate the build environment
#   make()  - Do the make operation for the platform/target
#   pkg()   - Wrap up the build artifacts for install
####################################
class BuildMgr
  def _cmd(cmd)
    $log.entry "running command: #{cmd}"
    system(cmd)

    ret = $?.exitstatus
    if ret != 0
      $log.entry "command had exit status: #{ret}\n\t\t#{cmd}"
      return BM_FAILURE
    end

    BM_SUCCESS
  end

  def tool_check
    tool_result = system("./scripts/check_tools.rb --platform #{@platform}")
    return BM_FAILURE unless tool_result==true
    BM_SUCCESS
  end

  def clean
    $log.entry "Starting CLEAN for #{@platform}"
    rc = BM_SUCCESS

    # Delete the build tree
    rc = _cmd("rm -fr #{@build_platform_dir}") if Dir.exist?(@build_platform_dir)
    return BM_FAILURE if not rc

    # If build-top is empty, delete it too
    rc = _cmd("rmdir #{@build_dir}") if Dir.exist?(@build_dir) && Dir.glob(@build_dir+"/*").empty?
    return BM_FAILURE if not rc

    $log.entry "Finished CLEAN for #{@platform}"
    return rc
  end

  def setup
    $log.entry "Starting SETUP for #{@platform}"
    rc = BM_SUCCESS

    #   Do we need to create build root?
    $log.entry "Looking for #{@build_dir}"
    rc = _cmd("mkdir #{@build_dir}") if not Dir.exist?(@build_dir)
    return BM_FAILURE if not rc

    #   Do we need to create a build-platform root
    rc = _cmd("mkdir #{@build_platform_dir}") if not Dir.exist?(@build_platform_dir)
    return BM_FAILURE if not rc

    cmake_opts =  " -DCMAKE_TOOLCHAIN_FILE=#{@cmake_toolchain}"
    cmake_opts +=  " -DBUILD_PLATFORM=#{@platform}"

    cmake_opts += " -DBUILD_MODE=Release" if @release_mode==true
    cmake_opts += " -DBUILD_VERBOSE=ON" if @verbose_mode==true

    if (@thread_count=="0")
    else
      cmake_opts += " -DCMAKE_CXX_COMPILER_LAUNCHER=ccache"
      cmake_opts += " -DCMAKE_C_COMPILER_LAUNCHER=ccache"
    end

    # Change directory to build-platform tree
    # Run CMake
    rc = _cmd("cd #{@build_platform_dir} && #{CMAKE} #{cmake_opts} #{@source_dir}")
    return BM_FAILURE if not rc

    $log.entry "Finished SETUP for #{@platform}"
    return rc
  end

  def make
    $log.entry "Starting MAKE for #{@platform}"

    # Verify Build Tree & Setup
    if not Dir.exist?(@build_platform_dir)
      $log.entry "#{@build_platform_dir} does not exist!  Run --setup first."
      return BM_FAILURE
    end

    if not File.exist?(@build_platform_dir+"/Makefile")
      $log.entry "#{@build_platform_dir} Makefile does not exist!  Run --setup first."
      return BM_FAILURE
    end

    # Change directory to build-platform
    # Run Make
    make_opts = ""
    if (@thread_count=="0")
      make_opts += ""
    else
      make_opts += "-j #{@thread_count}"
    end

    rc = _cmd("cd #{@build_platform_dir} && #{MAKE} #{make_opts}")
    return BM_FAILURE if not rc

    $log.entry "Finished MAKE for #{@platform}"
    BM_SUCCESS
  end

  def pkg
    $log.entry "Starting PKG for #{@platform}"

    # Go to output staging directory
    # Compress outputs to tarball

    $log.entry "Finished PKG for #{@platform}"
    BM_SUCCESS
  end

  def initialize(platform_tgt, release_mode, verbose_mode, thread_count)
    $log.entry "Starting build for #{platform_tgt}"
    @platform = platform_tgt
    @source_dir = $top
    @build_dir = $top+"/build"
    @build_platform_dir = @build_dir+"/"+platform_tgt

    @cmake_toolchain = @source_dir+"/cmake/"+@platform+".cmake"

    @release_mode = release_mode
    @verbose_mode = verbose_mode
    @thread_count = thread_count

  end
end

####################################
# Main processing, option parsing
####################################
if __FILE__ == $PROGRAM_NAME
  release_mode = false
  verbose_mode = false
  thread_count = "32"

  options = {}
  OptionParser.new do |opts|
    opts.banner = "usage:  enbuild.rb [options] --platform <build_platform>\n"

    opts.on('-p', '--platform [target]', "Platform to build. #{g_all_platforms} or \"all\"") do |arg|
      g_build_platforms = g_all_platforms if arg=='all'

      if g_all_platforms.include?(arg)
        g_build_platforms.push(arg)
      end
    end

    opts.on('-c', '--clean', "Destroy any existing build artifacts") do
      g_build_stages.push('clean')
    end

    opts.on('-s', '--setup', "Configure the build environment") do
      g_build_stages.push('setup')
    end

    opts.on('-m', '--make', "Create new build artifacts") do
      g_build_stages.push('make')
    end

    opts.on('-p', '--pkg', "Create an installation tarball") do
      g_build_stages.push('pkg')
    end

    opts.on('-r', '--release', "Build in release optimization") do
      release_mode = true
    end

    opts.on('-v', '--verbose', "Verbose build output.") do
      verbose_mode = true
    end

    opts.on("-j", "--jobs [job count]", Integer, "Number of make threads. default=32") do |jobs|
      thread_count=jobs
    end

  end.parse!


  if g_build_platforms.empty?
    g_build_platforms = g_all_platforms
  end

  if g_build_stages.empty?
    g_build_stages = g_all_stages
  end

  g_build_platforms.each do |platform_tgt|
    bm = BuildMgr.new(platform_tgt, release_mode, verbose_mode, thread_count)
    rc = BM_SUCCESS

    if bm.tool_check() == BM_FAILURE
      puts "Skipping Platform build: #{platform_tgt}"
      next
    end

    rc = bm.clean() if g_build_stages.include?('clean') && rc
    rc = bm.setup() if g_build_stages.include?('setup') && rc
    rc = bm.make() if g_build_stages.include?('make') && rc

    break if rc==BM_FAILURE
  end

end
