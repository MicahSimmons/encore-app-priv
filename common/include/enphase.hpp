// Copyright (c) 2017 Enphase Energy, Inc.  All rights reserved.
//
// enphase.hpp
//
// Common declarations across all applications.
//


#pragma once

#define ENP_OK    0
#define ENP_FAIL -1

