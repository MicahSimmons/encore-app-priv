// Copyright (c) 2017 Enphase Energy, Inc. All rights reserved.
//
// This file contains the public API definition for the
// GenericApp base class.
//
// GenericApp implements the minimal behaviors for an application.
// These behaviors are:
//    * Start a application main debug log.
//    * Handle any signals input to the application.
//    * Maintain a collection of service libraries.
//

#pragma once
#include <memory>

#include "libthread.hpp"

/* Forward declarations */
class GenericAppPriv;
typedef std::shared_ptr<GenericAppPriv> GenericAppPrivPtr;


class GenericApp {
public:
  GenericApp();

  /* start - Begin the main loop for the thread */
  virtual int start();
  
private:
  GenericAppPrivPtr _priv_ptr;
};
