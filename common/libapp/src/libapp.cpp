


#include "libapp.hpp"
#include "libapp_priv.hpp"

/* GenericApp constructor */
GenericApp::GenericApp() {
  _priv_ptr = std::make_shared<GenericAppPriv>();
  return;
}


int
GenericApp::start() {
  return 0;
}
