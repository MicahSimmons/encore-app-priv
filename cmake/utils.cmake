
function(msg arg)
  if (${BUILD_VERBOSE})
    message(${arg})
  endif()
endfunction()


function(include_cmn_and_deps arg)
  SET(_INCLUDE_PATH ${CMN}/${arg}/include)
  msg("Adding path: ${_INCLUDE_PATH}")
  include_directories(${_INCLUDE_PATH})

  if(EXISTS "${_INCLUDE_PATH}/header-deps.cmake")
    msg("Adding header-deps: ${_INCLUDE_PATH}/header-deps.cmake")    
    include(${_INCLUDE_PATH}/header-deps.cmake)
  endif()
endfunction()


